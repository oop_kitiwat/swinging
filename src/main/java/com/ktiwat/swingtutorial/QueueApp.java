package com.ktiwat.swingtutorial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel icon;
    JLabel lblQueueList, lblCurrent;
    JButton btnAddQ, btnGetQ, btnClearQ;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        queue = new LinkedList();

        this.setSize(400, 300);
        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
                }
            });
        btnAddQ = new JButton("Add Queue");
        btnAddQ.setBounds(250, 10, 100, 20);
        btnAddQ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnGetQ = new JButton("Get Queue");
        btnGetQ.setBounds(250, 40, 100, 20);
        btnGetQ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
                getQueue();
            }
        });

        btnClearQ = new JButton("Clear Queue");
        btnClearQ.setBounds(250, 70, 100, 20);
        btnClearQ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            clearQueue();    
        }
        }); 
        icon = new JLabel();
        icon.setBounds(30,120,200,100);
        icon.setIcon(new ImageIcon("Pto.jpg"));

        lblQueueList = new JLabel("Empty");
        lblQueueList.setBounds(30, 40, 200, 20);
        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(txtName);
        this.add(btnAddQ);
        this.add(btnGetQ);
        this.add(btnClearQ);
        this.add(lblQueueList);
        this.add(lblCurrent);
        this.add(icon);

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showQueue();
        this.setVisible(true);
    }

    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueueList.setText("Empty");
        } else {
            lblQueueList.setText(queue.toString());
        }
    }
    public void getQueue() {
        if(queue.isEmpty()) {
            lblCurrent.setText("????");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }
    public void clearQueue() {
        queue.clear();
        lblCurrent.setText("????");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp frame = new QueueApp();

    }

}
