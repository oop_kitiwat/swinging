package com.ktiwat.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame{
    JLabel lblName;
    JTextField txt;
    JButton btnHello;
    JLabel lblHello;
    public HelloMyName() {
        super("Hello My Name");
        lblName = new JLabel("Name: ");
        lblName.setBounds(10,10,50,20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);
        
        txt = new JTextField();
        txt.setBounds(70,10,180,20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(50,40,250,20);
        btnHello.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String myName = txt.getText();
           lblHello.setText("Hello " + myName);     
                
        }
            
        });

        lblHello = new JLabel("Hello");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(50,70,250,20);

        this.add(lblName);
        this.add(txt);
        this.add(btnHello);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
     HelloMyName frame = new HelloMyName();
}
    
}
